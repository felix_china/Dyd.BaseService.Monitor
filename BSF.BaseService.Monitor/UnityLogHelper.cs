﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSF.BaseService.Monitor.Dal;
using BSF.BaseService.Monitor.SystemRuntime;
using BSF.BaseService.Monitor.SystemRuntime.BatchQueues;

using BSF.Extensions;
using BSF.Config;
using BSF.Db;
using BSF.BaseService.Monitor.Base.Entity;
using BSF.BaseService.Monitor.Model;

namespace BSF.BaseService.Monitor
{
    public class UnityLogHelper
    {
        static LogBatchQueue logbatchqueue;
        static UnityLogHelper()
        {
            logbatchqueue = new LogBatchQueue();
        }

        public static void AddCommonLog(CommonLogInfo log)
        {
            if (BSFConfig.IsWriteCommonLog && BSFConfig.IsWriteCommonLogToMonitorPlatform)
            {
                try
                {
                    logbatchqueue.Add(log);
                }
                catch (Exception exp)
                {
                    BSF.Log.ErrorLog.Write("常用日志出错", exp);
                }
            }
        }

        public static void AddErrorLog(ErrorLogInfo log)
        {
            if (BSFConfig.IsWriteErrorLog && BSFConfig.IsWriteErrorLogToMonitorPlatform)
            {
                try
                {
                    if (!string.IsNullOrWhiteSpace(BSF.BaseService.Monitor.SystemRuntime.Config.UnityLogConnectString))
                    {
                        SqlHelper.ExcuteSql(BSF.BaseService.Monitor.SystemRuntime.Config.UnityLogConnectString, (c) =>
                        {
                            tb_error_log_dal errorlogdal = new tb_error_log_dal();
                            errorlogdal.Add(c, new tb_error_log_model(){
                             developer=log.developer,
                             logcreatetime=log.logcreatetime,
                             logtag=log.logtag,
                             logtype=log.logtype,
                             msg=log.msg,
                             projectname=log.projectname,
                             remark=log.remark,
                             sqlservercreatetime=log.sqlservercreatetime,
                             tracestack=log.tracestack
                            });
                        });
                        logbatchqueue.Add(new CommonLogInfo() { logcreatetime = log.logcreatetime, logtag = log.logtag, logtype = log.logtype, msg = log.msg.SubString2(900).NullToEmpty(), projectname = log.projectname });
                    }
                }
                catch (Exception exp)
                {
                    
                }
            }
        }
    }
}
